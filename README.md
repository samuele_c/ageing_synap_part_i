Data are stored in the `datasets` folder. Model code in the `code` folder. Data dictionaries for the datasets with a short description of each variable are provided in the `data_dictionaries` folder. There is one data dictionary file for each dataset file, the data dictionary filename consists of the dataset filename plus the suffix `_data_dictionary`. Running the model code creates an additional "results" directory in the root folder where this README is located. The descriptions given here and in the data dictionary files assume familiarity with the names of the variables and their abbreviations given in the paper.


Datasets:
---------

 - `ABR.csv`: ABR data
 - `audio_by_freq_no_ear.csv`: audiometric data (average of  right and left ears)
 - `demographic.csv`: demographic participant information (e.g. age, sex, etc...)
 - `descriptives.csv`: means and standard deviations of some variables (used to standardize variables in the analyses)
 - `FFR_ENV_av_across_mod_freq.csv`: FFR ENV data, averaged across the four different modulation frequencies
 - `FFR_latency_ENV`: group delay estimates of the FFR ENV latencies
 - `FFR_latency_TFSSL.csv`: group delay estimates of the latencies of FFR response to the lower side band of the AM tone (not analyzed further in the paper because of the high percentage of missing data)
 - `FFR_TFS_av_across_mod_freq.csv`: FFR TFS data, averaged across the four different modulation frequencies, and the two different modulation depths
 - `NIR.csv`: Cumulative lifetime noise exposure data

Code:
------------

The files `model_code_censored_robust_fixed_shrinkage.R`, and `model_code_robust_fixed_shrinkage.R` contain the JAGS model code for the ABR amplitude analyses, and for the FFR and ABR latencies analyses, respectively. The file `model_code_correlation.R` contains JAGS code for computing correlations among the predictor variables.

The files starting with `script_` contain the R code for analyzing the variables indicated in the suffix of the filename (e.g. `script_ABR_amplitude_HP_noise.R` contains the R code for the amplitudes of ABR responses with highpass masking noise).

The remaining files not described above contain utility functions as well as files used to generate figures and tables for the paper.

R, JAGS, and several R packages are needed to run the code. The packages needed are listed at the beginning of each script file (packages loaded with the `library` function). The `psychophysics`, and `ggsam` packages are available respectively at https://github.com/sam81/psychophysics and https://github.com/sam81/ggsam 


Notes:
-------------

Running the JAGS code can take a long time. For this reason the files invoking the JAGS functions have an `updateMCMC` switch that allows loading previously stored results instead of running the Markov chain Monte Carlo. If there are no previously stored results the scripts will throw an error message, so make sure `updateMCMC` is set to `TRUE` the first time you're running the scripts.

Inside the `code` folder there is a `session_info` folder containing the output of the `sessionInfo()` R command that prints version information about R, the OS and attached or loaded packages used to run the published analyses (the content of this folder will be overwritten if you run the analyses locally).
