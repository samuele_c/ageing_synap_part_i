getScalingFacs = function(dat, xNameCont=NULL, xNameCat=NULL, xNameInt=NULL, xNameIntVarType=NULL, yName="y", subjIDName=NULL,
                   sdBeta=rep(1, length(xName)),
                   numSavedSteps=10000 , thinSteps=1,
                   runjagsMethod="parallel",
                   nChains=4){


    ysd = sd(dat[,yName], na.rm=TRUE)
    ymean = mean(dat[,yName], na.rm=TRUE)
    zy = (dat[,yName]-ymean)/ysd
    nTotal = dim(dat)[1]
    nIndep = length(xNameCont) + length(xNameCat) + length(xNameInt)
    zx = matrix(nrow=nTotal, ncol=nIndep)
    scalingFacs = rep(NA, nIndep)
    varTypes = c(rep("cont", length(xNameCont)), rep("cat", length(xNameCat)), rep("int", length(xNameInt)))
    for (i in 1:length(xNameCont)){
        print(xNameCont)
        thisVals = dat[,xNameCont[i]]; thisMean=mean(thisVals); thisSD = sds[[xNameCont[i]]]
        zx[,i] = (thisVals-thisMean)/thisSD
        scalingFacs[i] = thisSD
    }
    cnt = length(xNameCont)

    if (is.null(xNameCat) == FALSE){
        print(xNameCat)
        for (i in 1:length(xNameCat)){
            thisVals = dat[,xNameCat[i]]
            zx[,(cnt+i)] = thisVals
            scalingFacs[(cnt+i)] = 1
        }
        cnt = length(xNameCont)+length(xNameCat)
    }

    if (is.null(xNameInt) == FALSE){
        for (i in 1:length(xNameInt)){
            thisVals = dat[,xNameInt[[i]]]
            thisScaleFacs = rep(NA, length(xNameInt[[i]]))
            for (j in 1:length(xNameInt[[i]])){
                if (xNameIntVarType[[i]][j] == "cont"){
                    tmpVals = dat[,xNameInt[[i]][j]]; thisMean=mean(tmpVals); thisSD = sds[[xNameInt[[i]][j]]]
                    thisVals[,j] = (tmpVals-thisMean)/thisSD
                    thisScaleFacs[j] = thisSD
                } else {
                    thisScaleFacs[j] = 1
                }
            }

            zx[,(cnt+i)] = apply(thisVals, 1, prod)
            scalingFacs[(cnt+i)] = prod(thisScaleFacs)
        }
    }

    return(scalingFacs)
    
}
