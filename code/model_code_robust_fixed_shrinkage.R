# Code adapted from:
#  Kruschke, J. K. (2015). Doing Bayesian Data Analysis, Second Edition:
#  A Tutorial with R, JAGS, and Stan. Academic Press / Elsevier.

library(runjags)
#===============================================================================
genMCMC = function(dat, xNameCont=NULL, xNameCat=NULL, xNameInt=NULL, xNameIntVarType=NULL, yName="y", subjIDName=NULL,
                   sdBeta=rep(1, length(xName)),
                   numSavedSteps=10000 , thinSteps=1,
                   runjagsMethod="parallel",
                   nChains=4){


    ysd = sd(dat[,yName], na.rm=TRUE)
    ymean = mean(dat[,yName], na.rm=TRUE)
    zy = (dat[,yName]-ymean)/ysd
    nTotal = dim(dat)[1]
    nIndep = length(xNameCont) + length(xNameCat) + length(xNameInt)
    zx = matrix(nrow=nTotal, ncol=nIndep)
    scalingFacs = rep(NA, nIndep)
    varTypes = c(rep("cont", length(xNameCont)), rep("cat", length(xNameCat)), rep("int", length(xNameInt)))
    for (i in 1:length(xNameCont)){
        print(xNameCont)
        thisVals = dat[,xNameCont[i]]; thisMean=mean(thisVals); thisSD = sds[[xNameCont[i]]]
        zx[,i] = (thisVals-thisMean)/thisSD
        scalingFacs[i] = thisSD
    }
    cnt = length(xNameCont)

    if (is.null(xNameCat) == FALSE){
        print(xNameCat)
        for (i in 1:length(xNameCat)){
            thisVals = dat[,xNameCat[i]]
            zx[,(cnt+i)] = thisVals
            scalingFacs[(cnt+i)] = 1
        }
        cnt = length(xNameCont)+length(xNameCat)
    }

    if (is.null(xNameInt) == FALSE){
        for (i in 1:length(xNameInt)){
            thisVals = dat[,xNameInt[[i]]]
            thisScaleFacs = rep(NA, length(xNameInt[[i]]))
            for (j in 1:length(xNameInt[[i]])){
                if (xNameIntVarType[[i]][j] == "cont"){
                    tmpVals = dat[,xNameInt[[i]][j]]; thisMean=mean(tmpVals); thisSD = sds[[xNameInt[[i]][j]]]
                    thisVals[,j] = (tmpVals-thisMean)/thisSD
                    thisScaleFacs[j] = thisSD
                } else {
                    thisScaleFacs[j] = 1
                }
            }

            zx[,(cnt+i)] = apply(thisVals, 1, prod)
            scalingFacs[(cnt+i)] = prod(thisScaleFacs)
        }
    }

    
    subjID = as.numeric(as.factor(dat[, subjIDName]))


    cat("\nCORRELATION MATRIX OF PREDICTORS:\n ")
    show( round(cor(zx),3) )
    cat("\n")
    flush.console()
    nX = dim(zx)[2]
    dataList = list(
        zx = zx,
        zy = zy,
        ymean = ymean,
        ysd=ysd,
        scalingFacs = scalingFacs,
        sdBeta = sdBeta,
        subjID=subjID,
        nSubj = length(unique(subjID)),
        nX = nX,
        nTotal = nTotal
    )
    ##-----------------------------------------------------------------------------
    ## THE MODEL.
    modelString = "

  # Specify the model for standardized data:
  model{
    for (i in 1:nTotal){
      zy[i] ~ dt(zbeta0 + sum(zbeta[1:nX] * zx[i,1:nX]) + zSubjEff[subjID[i]], 1/zSigma^2, nu)
    }
    # Priors vague on standardized scale:
    zbeta0 ~ dnorm(0, 1/2^2)
    for (j in 1:nX){
      zbeta[j] ~ dt(0, 1/sdBeta[j]^2, 1)
    }

    zSigma ~ dunif(1.0E-5, 1.0E+1)
    nu ~ dexp(1/30.0)

    # Transform to original *centered* scale:
    beta[1:nX] <- (zbeta[1:nX] / scalingFacs[1:nX])*ysd
    semizbeta[1:nX] <- (zbeta[1:nX])*ysd
    beta0 <- zbeta0*ysd + ymean
    sigma <- zSigma*ysd
    semizbeta0 <- zbeta0*ysd + ymean

    for (s in 2:nSubj){
      zSubjEff[s] ~ dnorm(0, 1/zSubjEffSigma^2)
    }
    zSubjEff[1] <- -sum(zSubjEff[2:nSubj]) #sum-to-zero constraint
    zSubjEffSigma ~ dunif(0.00001, 10)
    subjEffSigma <- zSubjEffSigma*ysd

  }
  " # close quote for modelString
    ## Write out modelString to a text file
    writeLines( modelString , con="TEMPmodel.txt" )
    ##-----------------------------------------------------------------------------
    ## INTIALIZE THE CHAINS.
    ## Let JAGS do it...

    ##-----------------------------------------------------------------------------
    ## RUN THE CHAINS
    parameters = c("beta0" ,  "beta" ,  "sigma", "semizbeta0" , "semizbeta",
                   "zbeta0" , "zbeta", "zSigma", "nu", "zSubjEff", "subjEffSigma")
    adaptSteps = 500  # Number of steps to "tune" the samplers
    burnInSteps = 1000
    initsFunction = function(chain){
        .RNG.seed = c(918, 536, 507, 414)[chain]
        .RNG.name = c("base::Super-Duper",
                      "base::Wichmann-Hill",
                      "base::Marsaglia-Multicarry",
                      "base::Mersenne-Twister")[chain]
        return(list(.RNG.seed=.RNG.seed,
                    .RNG.name=.RNG.name))
    }
    runJagsOut = run.jags( method=runjagsMethod,
                          model="TEMPmodel.txt",
                          monitor=parameters,
                          data=dataList,
                          inits=initsFunction,
                          n.chains=nChains,
                          adapt=adaptSteps,
                          burnin=burnInSteps,
                          sample=ceiling(numSavedSteps/nChains),
                          thin=thinSteps,
                          summarise=FALSE,
                          plots=FALSE)
    codaSamples = as.mcmc.list(runJagsOut)

    varNames = c(xNameCont, xNameCat)
    if (is.null(xNameInt) == FALSE){
        for (i in 1:length(xNameInt)){
            varNames = c(varNames, paste0(xNameInt[[i]], collapse="X"))
        }
    }

    for (i in 1:nChains){
        for (j in 1:length(varNames)){
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == "beta0")] = "Intercept"
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == "zbeta0")] = "z_Intercept"
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == "semizbeta0")] = "semiz_Intercept"
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == paste0("beta[", j, "]"))] = varNames[j]
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == paste0("zbeta[", j, "]"))] = paste0("z_", varNames[j])
            dimnames(codaSamples[[i]])[[2]][which(dimnames(codaSamples[[i]])[[2]] == paste0("semizbeta[", j, "]"))] = paste0("semiz_", varNames[j])
        }
    }

    return(list(codaSamples, runJagsOut, varNames))
}
