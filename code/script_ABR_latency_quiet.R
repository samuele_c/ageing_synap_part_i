source("global_parameters.R")
library(dplyr)
library(ggplot2)
library(ggsam)
library(psychophysics)
library(gtools)
##library(BayesUtils)
library(lmerTest)
library(emmeans)
library(xtable)
source("DBDA2E-utilities_DF.R")
source("DBDA2E-utilities_SC.R")
source("model_code_robust_fixed_shrinkage.R")


resDir = paste0(rootResDir, "model_ABR_latency_quiet/")
dir.create(resDir, recursive=T)
dir.create(paste0(resDir, "diagnostics/"), recursive=T)

##numSavedSteps=1000; thinSteps=10
numSavedSteps=40000; thinSteps=10
priorSDBeta = 0.1
updateMCMC = FALSE
credMass = 0.99

subjDatabaseCompl = read.table(paste0(rootDataDir, "demographic.csv"), sep=",", header=T)
dDescr = read.table(paste0(rootDataDir, "descriptives.csv"), sep=",", header=T)
dNIR = read.table(paste0(rootDataDir, "NIR.csv"), sep=",", header=T)
dAudio = read.table(paste0(rootDataDir, "audio_by_freq_no_ear.csv"), sep=",", header=T)

subjDatabaseCompl$ageDecade = subjDatabaseCompl$age/10
meanAgeDecade = mean(subjDatabaseCompl$ageDecade)
sdAgeDecade = sd(subjDatabaseCompl$ageDecade)
meanLog10NIR = mean(dNIR$log10Total)
sdLog10NIR = sd(dNIR$log10Total)


dABR = read.table(paste0(rootDataDir, "ABR.csv"), header=T, sep=",")
dABR$logPeakMeasureImputed = log(dABR$peakMeasureImputed)
dABR$logPeakMeasure = log(dABR$peakMeasure)
##dABR = dABR %>% filter(ear=="both")
dABR = left_join(dABR, subjDatabaseCompl %>% select(id, age, ageDecade, gender))
dABR = left_join(dABR, dAudio %>% select(id, audio0.5_2kHz=PTA0.5_2kHz, audio4_12kHz=PTA4_12kHz))
dABR = left_join(dABR, dNIR %>% select(id, log10NIR=log10Total))
dABR$log10NIRCent = dABR$log10NIR - mean(dABR$log10NIR)
dABR$id = factor(dABR$id)
dABR$id = factor(dABR$id, levels=mixedsort(levels(dABR$id))) 
dABRIV = dABR %>% filter(wave %in% c("I", "V")) %>% select(id, age, gender, audio0.5_2kHz, audio4_12kHz, montage, stim, cnd, wave, peakMeasureImputed, peakLatencyMeasure, logPeakMeasure, logPeakMeasureImputed, bin, dbinThresh, log10NIRCent, log10NIR)

dat = dABRIV %>% filter(montage %in% c("FzIpsiEarlobe", "FzIpsiTiptrode"), cnd=="quiet")

dat$stimDummy = ifelse(dat$stim == "lo", -1, 1)
dat$waveDummy = ifelse(dat$wave == "I", -1, 1)
dat$sexDummy = ifelse(dat$gender == "M", -1, 1)
dat$montageDummy = ifelse(dat$montage == "FzIpsiEarlobe", -1, 1) 
dat$ageDecade = dat$age/10
##dDescr = dat %>% group_by(stim, wave) %>% summarize(meanAudio0.5_12kHz=mean(audio0.5_12kHz), sdAudio0.5_12kHz=sd(audio0.5_12kHz), meanAudio0.5_2kHz=mean(audio0.5_2kHz), sdAudio0.5_2kHz=sd(audio0.5_2kHz), meanAudio4_12kHz=mean(audio4_12kHz), sdAudio4_12kHz=sd(audio4_12kHz))
dat$ageDecadeCent = dat$ageDecade - meanAgeDecade
##dat$audio0.5_12kHzCent = dat$audio0.5_12kHz - dDescr$meanAudio0.5_12kHz[1]
dat$audio0.5_2kHzCent = dat$audio0.5_2kHz - dDescr$meanPTA0.5_2kHz[1]
dat$audio4_12kHzCent = dat$audio4_12kHz - dDescr$meanPTA4_12kHz[1]


subjIDName = "id"
yName = "peakLatencyMeasure"

xNameCont = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "log10NIRCent")
sdBetaCont = rep(priorSDBeta, length(xNameCont))

xNameCat = c("stimDummy", "waveDummy", "montageDummy", "sexDummy")
sdBetaCat = c(10, 10, 10, 10)
    
xNameInt = list(c("stimDummy", "waveDummy"),
                c("stimDummy", "montageDummy"),
                c("stimDummy", "waveDummy", "montageDummy"),

                c("stimDummy", "sexDummy"),
                c("stimDummy", "waveDummy", "sexDummy"),
                
                c("stimDummy", "ageDecadeCent"),
                c("stimDummy", "audio0.5_2kHzCent"),
                c("stimDummy", "audio4_12kHzCent"),
                c("stimDummy", "log10NIRCent"),
                
                c("stimDummy", "waveDummy", "ageDecadeCent"),
                c("stimDummy", "waveDummy", "audio0.5_2kHzCent"),
                c("stimDummy", "waveDummy", "audio4_12kHzCent"),
                c("stimDummy", "waveDummy", "log10NIRCent"),

                c("waveDummy", "montageDummy"),
                c("waveDummy", "sexDummy"),

                c("waveDummy", "ageDecadeCent"),
                c("waveDummy", "audio0.5_2kHzCent"),
                c("waveDummy", "audio4_12kHzCent"),
                c("waveDummy", "log10NIRCent"),

                c("montageDummy", "ageDecadeCent"),
                c("montageDummy", "audio0.5_2kHzCent"),
                c("montageDummy", "audio4_12kHzCent"),
                c("montageDummy", "log10NIRCent"),

                c("stimDummy", "montageDummy", "ageDecadeCent"),
                c("stimDummy", "montageDummy", "audio0.5_2kHzCent"),
                c("stimDummy", "montageDummy", "audio4_12kHzCent"),
                c("stimDummy", "montageDummy", "log10NIRCent"),

                c("waveDummy", "montageDummy", "ageDecadeCent"),
                c("waveDummy", "montageDummy", "audio0.5_2kHzCent"),
                c("waveDummy", "montageDummy", "audio4_12kHzCent"),
                c("waveDummy", "montageDummy", "log10NIRCent"),

                c("stimDummy", "waveDummy", "montageDummy", "ageDecadeCent"),
                c("stimDummy", "waveDummy", "montageDummy", "audio0.5_2kHzCent"),
                c("stimDummy", "waveDummy", "montageDummy", "audio4_12kHzCent"),
                c("stimDummy", "waveDummy", "montageDummy", "log10NIRCent"),

                c("sexDummy", "audio0.5_2kHzCent"),
                c("sexDummy", "audio4_12kHzCent")
                
                )

xNameIntVarType = list(c("cat", "cat"),
                       c("cat", "cat"),
                       c("cat", "cat", "cat"),

                       c("cat", "cat"),
                       c("cat", "cat", "cat"),
                
                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),
                
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),

                       c("cat", "cat"),
                       c("cat", "cat"),

                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),

                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),

                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),

                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),
                       c("cat", "cat", "cont"),

                       c("cat", "cat", "cat", "cont"),
                       c("cat", "cat", "cat", "cont"),
                       c("cat", "cat", "cat", "cont"),
                       c("cat", "cat", "cat", "cont"),

                       c("cat", "cont"),
                       c("cat", "cont")
                       
                       )

sdBetaInt = rep(priorSDBeta, length(xNameInt))
sdBeta = c(sdBetaCont, sdBetaCat, sdBetaInt)

##

sds = list()
sds[["ageDecadeCent"]] = sdAgeDecade
sds[["audio0.5_2kHzCent"]] = dDescr$sdPTA0.5_2kHz[1]
sds[["audio4_12kHzCent"]] = dDescr$sdPTA4_12kHz[1]
sds[["log10NIRCent"]] = sdLog10NIR
sdy = sd(dat[,yName], na.rm=T)
dbinThresh = log(dat$dbinThresh[1])

if (updateMCMC == TRUE){
    out = genMCMC(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
                  xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
                  subjIDName=subjIDName,
                  #dbinName="bin", dbinThresh=dbinThresh,
                  sdBeta=sdBeta,
                  numSavedSteps=numSavedSteps, thinSteps=thinSteps)
    mcmcCoda = out[[1]]
    runJagsOut = out[[2]]
    varNames = out[[3]]
    saveRDS(mcmcCoda, file=paste0(resDir, "mcmcCoda.RDS"))
    saveRDS(runJagsOut, file=paste0(resDir, "runJagsOut.RDS"))
    saveRDS(varNames, file=paste0(resDir, "varNames.RDS"))

    parameterNames = varnames(mcmcCoda) # get all parameter names
    for (parName in parameterNames) {
        pdf(paste0(resDir, "diagnostics/", parName, ".pdf"))
        diagMCMC(codaObject=mcmcCoda , parName=parName)
        dev.off()
    }
} else {
    mcmcCoda = readRDS(file=paste0(resDir, "mcmcCoda.RDS"))
    runJagsOut = readRDS(file=paste0(resDir, "runJagsOut.RDS"))
    varNames = readRDS(file=paste0(resDir, "varNames.RDS"))
}

mcmcMat = as.matrix(mcmcCoda, chains=T)

b_Age = mcmcMat[, "ageDecadeCent"]
b_LxAge = mcmcMat[, "stimDummyXageDecadeCent"]
b_WxAge = mcmcMat[, "waveDummyXageDecadeCent"]
b_MxAge = mcmcMat[, "montageDummyXageDecadeCent"]
b_LxWxAge = mcmcMat[, "stimDummyXwaveDummyXageDecadeCent"]
b_LxMxAge = mcmcMat[, "stimDummyXmontageDummyXageDecadeCent"]
b_WxMxAge = mcmcMat[, "waveDummyXmontageDummyXageDecadeCent"]
b_LxWxMxAge = mcmcMat[, "stimDummyXwaveDummyXmontageDummyXageDecadeCent"]

b_AudL = mcmcMat[, "audio0.5_2kHzCent"]
b_LxAudL = mcmcMat[, "stimDummyXaudio0.5_2kHzCent"]
b_WxAudL = mcmcMat[, "waveDummyXaudio0.5_2kHzCent"]
b_MxAudL = mcmcMat[, "montageDummyXaudio0.5_2kHzCent"]
b_LxWxAudL = mcmcMat[, "stimDummyXwaveDummyXaudio0.5_2kHzCent"]
b_LxMxAudL = mcmcMat[, "stimDummyXmontageDummyXaudio0.5_2kHzCent"]
b_WxMxAudL =  mcmcMat[, "waveDummyXmontageDummyXaudio0.5_2kHzCent"]
b_LxWxMxAudL = mcmcMat[, "stimDummyXwaveDummyXmontageDummyXaudio0.5_2kHzCent"]

b_AudH = mcmcMat[, "audio4_12kHzCent"]
b_LxAudH = mcmcMat[, "stimDummyXaudio4_12kHzCent"]
b_WxAudH = mcmcMat[, "waveDummyXaudio4_12kHzCent"]
b_MxAudH = mcmcMat[, "montageDummyXaudio4_12kHzCent"]
b_LxWxAudH = mcmcMat[, "stimDummyXwaveDummyXaudio4_12kHzCent"]
b_LxMxAudH = mcmcMat[, "stimDummyXmontageDummyXaudio4_12kHzCent"]
b_WxMxAudH = mcmcMat[, "waveDummyXmontageDummyXaudio4_12kHzCent"]
b_LxWxMxAudH = mcmcMat[, "stimDummyXwaveDummyXmontageDummyXaudio4_12kHzCent"] 

b_NIR = mcmcMat[, "log10NIRCent"]
b_LxNIR = mcmcMat[, "stimDummyXlog10NIRCent"]
b_WxNIR = mcmcMat[, "waveDummyXlog10NIRCent"]
b_MxNIR = mcmcMat[, "montageDummyXlog10NIRCent"]
b_LxWxNIR = mcmcMat[, "stimDummyXwaveDummyXlog10NIRCent"]
b_LxMxNIR = mcmcMat[, "stimDummyXmontageDummyXlog10NIRCent"]
b_WxMxNIR = mcmcMat[, "waveDummyXmontageDummyXlog10NIRCent"]
b_LxWxMxNIR = mcmcMat[, "stimDummyXwaveDummyXmontageDummyXlog10NIRCent"]

b_M = mcmcMat[, "montageDummy"]
b_LxM = mcmcMat[, "stimDummyXmontageDummy"]
b_WxM = mcmcMat[, "waveDummyXmontageDummy"]
b_LxWxM = mcmcMat[, "stimDummyXwaveDummyXmontageDummy"]

b_Sex = mcmcMat[, "sexDummy"]
b_LxSex = mcmcMat[, "stimDummyXsexDummy"]
b_WxSex = mcmcMat[, "waveDummyXsexDummy"]
b_LxWxSex = mcmcMat[, "stimDummyXwaveDummyXsexDummy"]


W_I_lo_sex = 2*(b_Sex - b_LxSex - b_WxSex + b_LxWxSex)
W_I_hi_sex = 2*(b_Sex + b_LxSex - b_WxSex - b_LxWxSex)
W_V_lo_sex = 2*(b_Sex - b_LxSex + b_WxSex - b_LxWxSex)
W_V_hi_sex = 2*(b_Sex + b_LxSex + b_WxSex + b_LxWxSex)

W_I_lo_age_erl = b_Age - b_LxAge - b_WxAge - b_MxAge + b_LxWxAge + b_LxMxAge + b_WxMxAge - b_LxWxMxAge
W_I_hi_age_erl = b_Age + b_LxAge - b_WxAge - b_MxAge - b_LxWxAge - b_LxMxAge + b_WxMxAge + b_LxWxMxAge
W_V_lo_age_erl = b_Age - b_LxAge + b_WxAge - b_MxAge - b_LxWxAge + b_LxMxAge - b_WxMxAge + b_LxWxMxAge
W_V_hi_age_erl = b_Age + b_LxAge + b_WxAge - b_MxAge + b_LxWxAge - b_LxMxAge - b_WxMxAge - b_LxWxMxAge

W_I_lo_age_tpr = b_Age - b_LxAge - b_WxAge + b_MxAge + b_LxWxAge - b_LxMxAge - b_WxMxAge + b_LxWxMxAge
W_I_hi_age_tpr = b_Age + b_LxAge - b_WxAge + b_MxAge - b_LxWxAge + b_LxMxAge - b_WxMxAge - b_LxWxMxAge
W_V_lo_age_tpr = b_Age - b_LxAge + b_WxAge + b_MxAge - b_LxWxAge - b_LxMxAge + b_WxMxAge - b_LxWxMxAge
W_V_hi_age_tpr = b_Age + b_LxAge + b_WxAge + b_MxAge + b_LxWxAge + b_LxMxAge + b_WxMxAge + b_LxWxMxAge

W_I_lo_age = (W_I_lo_age_erl + W_I_lo_age_tpr)/2
W_I_hi_age = (W_I_hi_age_erl + W_I_hi_age_tpr)/2
W_V_lo_age = (W_V_lo_age_erl + W_V_lo_age_tpr)/2
W_V_hi_age = (W_V_hi_age_erl + W_V_hi_age_tpr)/2

W_I_lo_aud_lo_erl = b_AudL - b_LxAudL - b_WxAudL - b_MxAudL + b_LxWxAudL + b_LxMxAudL + b_WxMxAudL - b_LxWxMxAudL
W_I_hi_aud_lo_erl = b_AudL + b_LxAudL - b_WxAudL - b_MxAudL - b_LxWxAudL - b_LxMxAudL + b_WxMxAudL + b_LxWxMxAudL
W_V_lo_aud_lo_erl = b_AudL - b_LxAudL + b_WxAudL - b_MxAudL - b_LxWxAudL + b_LxMxAudL - b_WxMxAudL + b_LxWxMxAudL
W_V_hi_aud_lo_erl = b_AudL + b_LxAudL + b_WxAudL - b_MxAudL + b_LxWxAudL - b_LxMxAudL - b_WxMxAudL - b_LxWxMxAudL

W_I_lo_aud_lo_tpr = b_AudL - b_LxAudL - b_WxAudL + b_MxAudL + b_LxWxAudL - b_LxMxAudL - b_WxMxAudL + b_LxWxMxAudL
W_I_hi_aud_lo_tpr = b_AudL + b_LxAudL - b_WxAudL + b_MxAudL - b_LxWxAudL + b_LxMxAudL - b_WxMxAudL - b_LxWxMxAudL
W_V_lo_aud_lo_tpr = b_AudL - b_LxAudL + b_WxAudL + b_MxAudL - b_LxWxAudL - b_LxMxAudL + b_WxMxAudL - b_LxWxMxAudL
W_V_hi_aud_lo_tpr = b_AudL + b_LxAudL + b_WxAudL + b_MxAudL + b_LxWxAudL + b_LxMxAudL + b_WxMxAudL + b_LxWxMxAudL

W_I_lo_aud_lo = (W_I_lo_aud_lo_erl + W_I_lo_aud_lo_tpr)/2
W_I_hi_aud_lo = (W_I_hi_aud_lo_erl + W_I_hi_aud_lo_tpr)/2
W_V_lo_aud_lo = (W_V_lo_aud_lo_erl + W_V_lo_aud_lo_tpr)/2
W_V_hi_aud_lo = (W_V_hi_aud_lo_erl + W_V_hi_aud_lo_tpr)/2

W_I_lo_aud_hi_erl = b_AudH - b_LxAudH - b_WxAudH - b_MxAudH + b_LxWxAudH + b_LxMxAudH + b_WxMxAudH - b_LxWxMxAudH
W_I_hi_aud_hi_erl = b_AudH + b_LxAudH - b_WxAudH - b_MxAudH - b_LxWxAudH - b_LxMxAudH + b_WxMxAudH + b_LxWxMxAudH
W_V_lo_aud_hi_erl = b_AudH - b_LxAudH + b_WxAudH - b_MxAudH - b_LxWxAudH + b_LxMxAudH - b_WxMxAudH + b_LxWxMxAudH
W_V_hi_aud_hi_erl = b_AudH + b_LxAudH + b_WxAudH - b_MxAudH + b_LxWxAudH - b_LxMxAudH - b_WxMxAudH - b_LxWxMxAudH

W_I_lo_aud_hi_tpr = b_AudH - b_LxAudH - b_WxAudH + b_MxAudH + b_LxWxAudH - b_LxMxAudH - b_WxMxAudH + b_LxWxMxAudH
W_I_hi_aud_hi_tpr = b_AudH + b_LxAudH - b_WxAudH + b_MxAudH - b_LxWxAudH + b_LxMxAudH - b_WxMxAudH - b_LxWxMxAudH
W_V_lo_aud_hi_tpr = b_AudH - b_LxAudH + b_WxAudH + b_MxAudH - b_LxWxAudH - b_LxMxAudH + b_WxMxAudH - b_LxWxMxAudH
W_V_hi_aud_hi_tpr = b_AudH + b_LxAudH + b_WxAudH + b_MxAudH + b_LxWxAudH + b_LxMxAudH + b_WxMxAudH + b_LxWxMxAudH

W_I_lo_aud_hi = (W_I_lo_aud_hi_erl + W_I_lo_aud_hi_tpr)/2
W_I_hi_aud_hi = (W_I_hi_aud_hi_erl + W_I_hi_aud_hi_tpr)/2
W_V_lo_aud_hi = (W_V_lo_aud_hi_erl + W_V_lo_aud_hi_tpr)/2
W_V_hi_aud_hi = (W_V_hi_aud_hi_erl + W_V_hi_aud_hi_tpr)/2

W_I_lo_NIR_erl = b_NIR - b_LxNIR - b_WxNIR - b_MxNIR + b_LxWxNIR + b_LxMxNIR + b_WxMxNIR - b_LxWxMxNIR
W_I_hi_NIR_erl = b_NIR + b_LxNIR - b_WxNIR - b_MxNIR - b_LxWxNIR - b_LxMxNIR + b_WxMxNIR + b_LxWxMxNIR
W_V_lo_NIR_erl = b_NIR - b_LxNIR + b_WxNIR - b_MxNIR - b_LxWxNIR + b_LxMxNIR - b_WxMxNIR + b_LxWxMxNIR
W_V_hi_NIR_erl = b_NIR + b_LxNIR + b_WxNIR - b_MxNIR + b_LxWxNIR - b_LxMxNIR - b_WxMxNIR - b_LxWxMxNIR

W_I_lo_NIR_tpr = b_NIR - b_LxNIR - b_WxNIR + b_MxNIR + b_LxWxNIR - b_LxMxNIR - b_WxMxNIR + b_LxWxMxNIR
W_I_hi_NIR_tpr = b_NIR + b_LxNIR - b_WxNIR + b_MxNIR - b_LxWxNIR + b_LxMxNIR - b_WxMxNIR - b_LxWxMxNIR
W_V_lo_NIR_tpr = b_NIR - b_LxNIR + b_WxNIR + b_MxNIR - b_LxWxNIR - b_LxMxNIR + b_WxMxNIR - b_LxWxMxNIR
W_V_hi_NIR_tpr = b_NIR + b_LxNIR + b_WxNIR + b_MxNIR + b_LxWxNIR + b_LxMxNIR + b_WxMxNIR + b_LxWxMxNIR

W_I_lo_NIR = (W_I_lo_NIR_erl + W_I_lo_NIR_tpr)/2
W_I_hi_NIR = (W_I_hi_NIR_erl + W_I_hi_NIR_tpr)/2
W_V_lo_NIR = (W_V_lo_NIR_erl + W_V_lo_NIR_tpr)/2
W_V_hi_NIR = (W_V_hi_NIR_erl + W_V_hi_NIR_tpr)/2

W_I_hi_W_I_lo_diff_age_erl = W_I_hi_age_erl - W_I_lo_age_erl
W_I_hi_W_I_lo_diff_aud_lo_erl = W_I_hi_aud_lo_erl - W_I_lo_aud_lo_erl
W_I_hi_W_I_lo_diff_aud_hi_erl = W_I_hi_aud_hi_erl - W_I_lo_aud_hi_erl
W_I_hi_W_I_lo_diff_NIR_erl = W_I_hi_NIR_erl - W_I_lo_NIR_erl

W_I_hi_W_I_lo_diff_age_tpr = W_I_hi_age_tpr - W_I_lo_age_tpr
W_I_hi_W_I_lo_diff_aud_lo_tpr = W_I_hi_aud_lo_tpr - W_I_lo_aud_lo_tpr
W_I_hi_W_I_lo_diff_aud_hi_tpr = W_I_hi_aud_hi_tpr - W_I_lo_aud_hi_tpr
W_I_hi_W_I_lo_diff_NIR_tpr = W_I_hi_NIR_tpr - W_I_lo_NIR_tpr

W_I_hi_W_I_lo_diff_age = W_I_hi_age - W_I_lo_age
W_I_hi_W_I_lo_diff_aud_lo = W_I_hi_aud_lo - W_I_lo_aud_lo
W_I_hi_W_I_lo_diff_aud_hi = W_I_hi_aud_hi - W_I_lo_aud_hi
W_I_hi_W_I_lo_diff_NIR = W_I_hi_NIR - W_I_lo_NIR


pdf(paste0(resDir, "posterior.pdf"))
par(mfrow=c(2,3))
plotPost(mcmcMat[, "Intercept"], main="Intercept")
for (i in 1:length(varNames)){
    plotPost(mcmcMat[, varNames[i]], main=varNames[i], compVal=0, credMass=credMass, cex.main=0.65)
}
dev.off()

pdf(paste0(resDir, "posterior_z.pdf"))
par(mfrow=c(2,3))
plotPost(mcmcMat[, "z_Intercept"], main="Intercept")
for (i in 1:length(varNames)){
    plotPost(mcmcMat[, paste0("z_", varNames[i])], main=varNames[i], compVal=0, credMass=credMass, cex.main=0.65)
}
dev.off()

pdf(paste0(resDir, "posterior.pdf"))
par(mfrow=c(2,3))
for (i in 1:length(varNames)){
    plotPost(mcmcMat[, varNames[i]], main=varNames[i], compVal=0, credMass=credMass, cex.main=0.65)
}
dev.off()



pdf(paste0(resDir, "posterior_effects_by_subcond.pdf"), width=12, height=5)
par(mfrow=c(2,4))
plotPost(W_I_lo_age, main="Age W I lo", compVal=0, credMass=credMass)
plotPost(W_I_hi_age, main="Age W I hi", compVal=0, credMass=credMass)
plotPost(W_V_lo_age, main="Age W V lo", compVal=0, credMass=credMass) 
plotPost(W_V_hi_age, main="Age W V hi", compVal=0, credMass=credMass)

plotPost(W_I_lo_aud_lo, main="Aud Low Freq W I lo", compVal=0, credMass=credMass)
plotPost(W_I_hi_aud_lo, main="Aud Low Freq W I hi", compVal=0, credMass=credMass)
plotPost(W_V_lo_aud_lo, main="Aud Low Freq W V lo", compVal=0, credMass=credMass)
plotPost(W_V_hi_aud_lo, main="Aud Low Freq W V hi", compVal=0, credMass=credMass)

plotPost(W_I_lo_aud_hi, main="Aud High Freq W I lo", compVal=0, credMass=credMass)
plotPost(W_I_hi_aud_hi, main="Aud High Freq W I hi", compVal=0, credMass=credMass)
plotPost(W_V_lo_aud_hi, main="Aud High Freq W V lo", compVal=0, credMass=credMass)
plotPost(W_V_hi_aud_hi, main="Aud High Freq W V hi", compVal=0, credMass=credMass)

plotPost(W_I_lo_NIR, main="NIR W I lo", compVal=0, credMass=credMass)
plotPost(W_I_hi_NIR, main="NIR W I hi", compVal=0, credMass=credMass)
plotPost(W_V_lo_NIR, main="NIR W V lo", compVal=0, credMass=credMass) 
plotPost(W_V_hi_NIR, main="NIR W V hi", compVal=0, credMass=credMass)

plotPost(W_I_lo_sex, main="sex W I lo", compVal=0, credMass=credMass)
plotPost(W_I_hi_sex, main="sex W I hi", compVal=0, credMass=credMass)
plotPost(W_V_lo_sex, main="sex W V lo", compVal=0, credMass=credMass) 
plotPost(W_V_hi_sex, main="sex W V hi", compVal=0, credMass=credMass)

dev.off()

## #######################
## Summary Plots
## #######################
dodgeSize = -0.75

lev_fac = rep(c("80 dB", "105 dB"), 9)
wave_fac = rep(c("Wave I", "Wave I", "Wave V", "Wave V", "Wave V - Wave I", "Wave V - Wave I"), 3)
lev_levs = c("80 dB", "105 dB")
wave_levs = c("Wave I", "Wave V", "Wave V - Wave I")
lbls = c("I 80dB", "I 105dB", "V 80dB", "V 105dB", "I 105dB / I 80dB")
## lbls = c(paste0("Overall ", lbls), paste0("Earlobe ", lbls), paste0("Tiptrode ", lbls))
mnt_fac = c(rep("Overall", 6), rep("HF-IERL", 6), rep("HF-ITPR", 6))
mnt_levs = c("Overall", "HF-IERL", "HF-ITPR")

df_age_post = summarizePostListDF(list(W_I_lo_age, W_I_hi_age, W_V_lo_age, W_V_hi_age, W_V_lo_age - W_I_lo_age, W_V_hi_age - W_I_hi_age,
W_I_lo_age_erl, W_I_hi_age_erl, W_V_lo_age_erl, W_V_hi_age_erl, W_V_lo_age_erl - W_I_lo_age_erl, W_V_hi_age_erl - W_I_hi_age_erl,
W_I_lo_age_tpr, W_I_hi_age_tpr, W_V_lo_age_tpr, W_V_hi_age_tpr, W_V_lo_age_tpr - W_I_lo_age_tpr, W_V_hi_age_tpr - W_I_hi_age_tpr),
labels=c("W_I_lo_age", "W_I_hi_age", "W_V_lo_age", "W_V_hi_age", "W_V_lo_age - W_I_lo_age", "W_V_hi_age - W_I_hi_age",
"W_I_lo_age_erl", "W_I_hi_age_erl", "W_V_lo_age_erl", "W_V_hi_age_erl", "W_V_lo_age_erl - W_I_lo_age_erl", "W_V_hi_age_erl - W_I_hi_age_erl",
"W_I_lo_age_tpr", "W_I_hi_age_tpr", "W_V_lo_age_tpr", "W_V_hi_age_tpr", "W_V_lo_age_tpr - W_I_lo_age_tpr", "W_V_hi_age_tpr - W_I_hi_age_tpr"), credMass=credMass)
df_age_post$lev = factor(lev_fac, levels= lev_levs)
df_age_post$wave = factor(wave_fac, levels=wave_levs)
df_age_post$montage = factor(mnt_fac, levels=mnt_levs)

p = ggplot(df_age_post, aes(x=lev, y=Median, color=montage)) + facet_wrap(~wave)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0, position=position_dodge(dodgeSize))
p = p + geom_point(shape=1, size=2, position=position_dodge(dodgeSize))
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bwng() + theme(axis.title.x = element_text(hjust=1))
p = p + ylab("Latency Change (ms) X Age Decade") + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
p = p + theme(plot.title = element_text(hjust = 1))
ggsave(filename=paste0(resDir, "HDIs_quiet_age.pdf"), width=size2c, height=size1c)


df_aud_lo_post = summarizePostListDF(list(W_I_lo_aud_lo, W_I_hi_aud_lo, W_V_lo_aud_lo, W_V_hi_aud_lo, W_V_lo_aud_lo - W_I_lo_aud_lo, W_V_hi_aud_lo - W_I_hi_aud_lo,
W_I_lo_aud_lo_erl, W_I_hi_aud_lo_erl, W_V_lo_aud_lo_erl, W_V_hi_aud_lo_erl, W_V_lo_aud_lo_erl - W_I_lo_aud_lo_erl, W_V_hi_aud_lo_erl - W_I_hi_aud_lo_erl,
W_I_lo_aud_lo_tpr, W_I_hi_aud_lo_tpr, W_V_lo_aud_lo_tpr, W_V_hi_aud_lo_tpr, W_V_lo_aud_lo_tpr - W_I_lo_aud_lo_tpr, W_V_hi_aud_lo_tpr - W_I_hi_aud_lo_tpr),
labels=c("W_I_lo_aud_lo", "W_I_hi_aud_lo", "W_V_lo_aud_lo", "W_V_hi_aud_lo", "W_V_lo_aud_lo - W_I_lo_aud_lo", "W_V_hi_aud_lo - W_I_hi_aud_lo",
"W_I_lo_aud_lo_erl", "W_I_hi_aud_lo_erl", "W_V_lo_aud_lo_erl", "W_V_hi_aud_lo_erl", "W_V_lo_aud_lo_erl - W_I_lo_aud_lo_erl", "W_V_hi_aud_lo_erl - W_I_hi_aud_lo_erl",
"W_I_lo_aud_lo_tpr", "W_I_hi_aud_lo_tpr", "W_V_lo_aud_lo_tpr", "W_V_hi_aud_lo_tpr", "W_V_lo_aud_lo_tpr - W_I_lo_aud_lo_tpr", "W_V_hi_aud_lo_tpr - W_I_hi_aud_lo_tpr"), credMass=credMass)
df_aud_lo_post$lev = factor(lev_fac, levels= lev_levs)
df_aud_lo_post$wave = factor(wave_fac, levels=wave_levs)
df_aud_lo_post$montage = factor(mnt_fac, levels=mnt_levs)

p = ggplot(df_aud_lo_post, aes(x=lev, y=Median, color=montage)) + facet_wrap(~wave)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0, position=position_dodge(dodgeSize))
p = p + geom_point(shape=1, size=2, position=position_dodge(dodgeSize))
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bwng() + theme(axis.title.x = element_text(hjust=1)) 
p = p + ylab(expression("Latency Change (ms) X PTA"["0.5-2"]*" (dB)")) + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
p = p + theme(plot.title = element_text(hjust = 1))
ggsave(filename=paste0(resDir, "HDIs_quiet_aud_lo.pdf"), width=size2c, height=size1c)

##
df_aud_hi_post = summarizePostListDF(list(W_I_lo_aud_hi, W_I_hi_aud_hi, W_V_lo_aud_hi, W_V_hi_aud_hi, W_V_lo_aud_hi - W_I_lo_aud_hi, W_V_hi_aud_hi - W_I_hi_aud_hi,
W_I_lo_aud_hi_erl, W_I_hi_aud_hi_erl, W_V_lo_aud_hi_erl, W_V_hi_aud_hi_erl, W_V_lo_aud_hi_erl - W_I_lo_aud_hi_erl, W_V_hi_aud_hi_erl - W_I_hi_aud_hi_erl,
W_I_lo_aud_hi_tpr, W_I_hi_aud_hi_tpr, W_V_lo_aud_hi_tpr, W_V_hi_aud_hi_tpr, W_V_lo_aud_hi_tpr - W_I_lo_aud_hi_tpr, W_V_hi_aud_hi_tpr - W_I_hi_aud_hi_tpr),
labels=c("W_I_lo_aud_hi", "W_I_hi_aud_hi", "W_V_lo_aud_hi", "W_V_hi_aud_hi", "W_V_lo_aud_hi - W_I_lo_aud_hi", "W_V_hi_aud_hi - W_I_hi_aud_hi",
"W_I_lo_aud_hi_erl", "W_I_hi_aud_hi_erl", "W_V_lo_aud_hi_erl", "W_V_hi_aud_hi_erl", "W_V_lo_aud_hi_erl - W_I_lo_aud_hi_erl", "W_V_hi_aud_hi_erl - W_I_hi_aud_hi_erl",
"W_I_lo_aud_hi_tpr", "W_I_hi_aud_hi_tpr", "W_V_lo_aud_hi_tpr", "W_V_hi_aud_hi_tpr", "W_V_lo_aud_hi_tpr - W_I_lo_aud_hi_tpr", "W_V_hi_aud_hi_tpr - W_I_hi_aud_hi_tpr"), credMass=credMass)
df_aud_hi_post$lev = factor(lev_fac, levels= lev_levs)
df_aud_hi_post$wave = factor(wave_fac, levels=wave_levs)
df_aud_hi_post$montage = factor(mnt_fac, levels=mnt_levs)

p = ggplot(df_aud_hi_post, aes(x=lev, y=Median, color=montage)) + facet_wrap(~wave)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0, position=position_dodge(dodgeSize))
p = p + geom_point(shape=1, size=2, position=position_dodge(dodgeSize))
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bwng() + theme(axis.title.x = element_text(hjust=1)) 
p = p + ylab(expression("Latency Change (ms) X PTA"["0.5-2"]*" (dB)")) + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
p = p + theme(plot.title = element_text(hjust = 1))
ggsave(filename=paste0(resDir, "HDIs_quiet_aud_hi.pdf"), width=size2c, height=size1c)


df_NIR_post = summarizePostListDF(list(W_I_lo_NIR, W_I_hi_NIR, W_V_lo_NIR, W_V_hi_NIR, W_V_lo_NIR - W_I_lo_NIR, W_V_hi_NIR - W_I_hi_NIR,
W_I_lo_NIR_erl, W_I_hi_NIR_erl, W_V_lo_NIR_erl, W_V_hi_NIR_erl, W_V_lo_NIR_erl - W_I_lo_NIR_erl, W_V_hi_NIR_erl - W_I_hi_NIR_erl,
W_I_lo_NIR_tpr, W_I_hi_NIR_tpr, W_V_lo_NIR_tpr, W_V_hi_NIR_tpr, W_V_lo_NIR_tpr - W_I_lo_NIR_tpr, W_V_hi_NIR_tpr - W_I_hi_NIR_tpr),
labels=c("W_I_lo_NIR", "W_I_hi_NIR", "W_V_lo_NIR", "W_V_hi_NIR", "W_V_lo_NIR - W_I_lo_NIR", "W_V_hi_NIR - W_I_hi_NIR",
"W_I_lo_NIR_erl", "W_I_hi_NIR_erl", "W_V_lo_NIR_erl", "W_V_hi_NIR_erl", "W_V_lo_NIR_erl - W_I_lo_NIR_erl", "W_V_hi_NIR_erl - W_I_hi_NIR_erl",
"W_I_lo_NIR_tpr", "W_I_hi_NIR_tpr", "W_V_lo_NIR_tpr", "W_V_hi_NIR_tpr", "W_V_lo_NIR_tpr - W_I_lo_NIR_tpr", "W_V_hi_NIR_tpr - W_I_hi_NIR_tpr"), credMass=credMass)
df_NIR_post$lev = factor(lev_fac, levels= lev_levs)
df_NIR_post$wave = factor(wave_fac, levels=wave_levs)
df_NIR_post$montage = factor(mnt_fac, levels=mnt_levs)

p = ggplot(df_NIR_post, aes(x=lev, y=Median, color=montage)) + facet_wrap(~wave)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0, position=position_dodge(dodgeSize))
p = p + geom_point(shape=1, size=2, position=position_dodge(dodgeSize))
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bwng() + theme(axis.title.x = element_text(hjust=1)) 
p = p + ylab("Latency Change (ms) X log10 NIR") + xlab(NULL) 
p = p + theme(plot.title = element_text(hjust = 1))
ggsave(filename=paste0(resDir, "HDIs_quiet_NIR.pdf"), width=size2c, height=size1c)


df_sex_post = summarizePostListDF(list(W_I_lo_sex, W_I_hi_sex, W_V_lo_sex, W_V_hi_sex),
labels=c("I 80dB", "I 105dB", "V 80dB", "V 105dB"), credMass=credMass)
df_sex_post$lev = factor(c("80 dB", "105 dB", "80 dB", "105 dB"), levels=c("80 dB", "105 dB"))
df_sex_post$wave = factor(c("Wave I", "Wave I", "Wave V", "Wave V"), levels=c("Wave I", "Wave V"))

p = ggplot(df_sex_post, aes(x=label, y=Median)) 
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
p = p + geom_point(shape=1, size=2)
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bwng() + theme(axis.title.x = element_text(hjust=1))
p = p + ylab("% Amplitude Difference (Female - Male)") + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
p = p + theme(plot.title = element_text(hjust = 1))
ggsave(filename=paste0(resDir, "HDIs_quiet_sex.pdf"), width=3.4252*1.2, height=3)


write.table(df_age_post, paste0(resDir, "HDIs_age.csv"), col.names=T, row.names=F, sep=",")
write.table(df_aud_lo_post, paste0(resDir, "HDIs_aud_lo.csv"), col.names=T, row.names=F, sep=",")
write.table(df_aud_hi_post, paste0(resDir, "HDIs_aud_hi.csv"), col.names=T, row.names=F, sep=",")
write.table(df_NIR_post, paste0(resDir, "HDIs_NIR.csv"), col.names=T, row.names=F, sep=",")
write.table(df_sex_post, paste0(resDir, "HDIs_sex.csv"), col.names=T, row.names=F, sep=",")

writeLines(capture.output(sessionInfo()), "session_info/session_info_ABR_latency_quiet.txt")

## ## ## lme4
## emm_options(pbkrtest.limit = 3264)
## options(contrasts=c('contr.sum','contr.poly'))

## fit1 = lmer(logPeakMeasure ~ stimDummy*waveDummy*ageDecadeCent +
##                 stimDummy*waveDummy*audio0.5_2kHzCent +
##                 stimDummy*waveDummy*audio4_12kHzCent +
##                 stimDummy*waveDummy*log10NIRCent +
##                 stimDummy*waveDummy*sexDummy +
##                 sexDummy*audio0.5_2kHzCent +
##                 sexDummy*audio4_12kHzCent +
##                 (1|id), data=dat)
## fit1 = lmer(logPeakMeasureImputed ~ stim*wave*montage*ageDecadeCent +
##                 stim*wave*montage*audio0.5_2kHzCent +
##                 stim*wave*montage*audio4_12kHzCent +
##                 stim*wave*montage*log10NIRCent +
##                 stim*wave*gender +
##                 gender*audio0.5_2kHzCent +
##                 gender*audio4_12kHzCent +
##                 (1|id), data=dat)
## ## do pairwise ~ factorName if you want the contrasts
##emtrends(fit1, pairwise ~ stim+wave+montage, var = "ageDecadeCent", adjust="none")
## ageTrendsByCarrFreqXmodAmp = emtrends(fit1, ~ carrFreqDummy+modAmpDummy, var = "ageDecadeCent")
## CIsAgeTrendsByCarrFreqXmodAmp = confint(ageTrendsByCarrFreqXmodAmp, level=0.99)
## noiseTrendsByCarrFreqXmodAmp = emtrends(fit1, ~ carrFreqDummy+modAmpDummy, var = "log10NIRCent")
## CIsNoiseTrendsByCarrFreqXmodAmp = confint(noiseTrendsByCarrFreqXmodAmp, level=0.99)


## source("getScalingFacs.R")

## scalFacs = getScalingFacs(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
##                   xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
##                   subjIDName=subjIDName,
##                   sdBeta=sdBeta,
##                   numSavedSteps=numSavedSteps, thinSteps=thinSteps)

## nVars = length(varNames)
## vNamesArr = character(nVars)
## cenTendArr = numeric(nVars)
## CIArr = character(nVars)
## nDig = 3
## zsdBetaArr = c(sdBetaCont, sdBetaCat, sdBetaInt)
## pType = c(rep("Cont.", length(xNameCont)), rep("Cat.", length(xNameCat)), rep("Int.", length(xNameInt)))
## for (vn in 1:length(varNames)){
##     thisVar = varNames[vn]
##     thisPost = summarizePost(mcmcMat[,thisVar], credMass=credMass)
##     vNamesArr[vn] = thisVar
##     cenTendArr[vn] = thisPost[["Median"]]
##     CIArr[vn] = paste0(as.character(round(thisPost[["HDIlow"]], nDig)), "--", as.character(round(thisPost[["HDIhigh"]], nDig)))
## }

## ptns = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "log10NIRCent", "stimDummy", "waveDummy", "montageDummy", "sexDummy", "X")
## rpls = c("A", "PTA0.5--2", "PTA4--12", "log10TCNE", "L", "W", "M", "S", "x")
## for (i in 1:length(ptns)){
##     vNamesArr = gsub(ptns[i], rpls[i], vNamesArr)
## }
## df_coeffs = data.frame(varName=vNamesArr, pType, zsdBeta=zsdBetaArr, sdBeta=zsdBetaArr/scalFacs*sdy, Median=cenTendArr, CI=CIArr)

## xt = xtable(df_coeffs, digits=3)
## writeLines(print(xt), paste0(resDir, "coeffs.tex"))

source("getScalingFacs.R")

scalFacs = getScalingFacs(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
                  xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
                  subjIDName=subjIDName,
                  sdBeta=sdBeta,
                  numSavedSteps=numSavedSteps, thinSteps=thinSteps)

varNames2 = c("Intercept", varNames)
nVars = length(varNames2)
vNamesArr = character(nVars)
cenTendArr = numeric(nVars)
CIArr = character(nVars)
nDig = 3
zsdBetaArr = c(sdBetaCont, sdBetaCat, sdBetaInt)
pType = c("", rep("Cont.", length(xNameCont)), rep("Cat.", length(xNameCat)), rep("Int.", length(xNameInt)))
for (vn in 1:nVars){
    thisVar = varNames2[vn]
    thisPost = summarizePost(mcmcMat[,thisVar], credMass=credMass)
    vNamesArr[vn] = thisVar
    cenTendArr[vn] = thisPost[["Median"]]
    CIArr[vn] = paste0(as.character(round(thisPost[["HDIlow"]], nDig)), "--", as.character(round(thisPost[["HDIhigh"]], nDig)))
}

ptns = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "audio1_2kHzCent", "log10NIRCent", "stimDummy", "waveDummy", "montageDummy", "montDummy", "sexDummy", "X", "carrFreqDummy", "modAmpDummy", "musicYearsCubeRootCent")
rpls = c("A", "PTA0.5--2", "PTA4--12", "PTA1--2", "log10TCNE", "L", "W", "M", "M", "S", "x", "CF", "MA", "MY")
for (i in 1:length(ptns)){
    vNamesArr = gsub(ptns[i], rpls[i], vNamesArr)
}
df_coeffs = data.frame(varName=vNamesArr, pType, zsdBeta=c(2, zsdBetaArr), sdBeta=c(2*sdy, zsdBetaArr/scalFacs*sdy), Median=cenTendArr, CI=CIArr)

xt = xtable(df_coeffs, digits=3)
writeLines(print(xt, include.rownames=FALSE), paste0(resDir, "coeffs.tex"))

b_L = mcmcMat[, "stimDummy"]
b_W = mcmcMat[, "waveDummy"]
b_LxW = mcmcMat[, "stimDummyXwaveDummy"]

## W I hi - lo latency difference
plotPost((b_L-b_LxW)*2, credMass=credMass)

## W V hi - lo latency difference
plotPost((b_L+b_LxW)*2, credMass=credMass)


## DIAGNOSTICS

d_I_HL_ERL = dat %>% filter(wave=="I", stim=="hi", montage=="FzIpsiEarlobe")
d_I_HL_TPR = dat %>% filter(wave=="I", stim=="hi", montage=="FzIpsiTiptrode")
d_I_LL_ERL = dat %>% filter(wave=="I", stim=="lo", montage=="FzIpsiEarlobe")
d_I_LL_TPR = dat %>% filter(wave=="I", stim=="lo", montage=="FzIpsiTiptrode")

d_V_HL_ERL = dat %>% filter(wave=="V", stim=="hi", montage=="FzIpsiEarlobe")
d_V_HL_TPR = dat %>% filter(wave=="V", stim=="hi", montage=="FzIpsiTiptrode")
d_V_LL_ERL = dat %>% filter(wave=="V", stim=="lo", montage=="FzIpsiEarlobe")
d_V_LL_TPR = dat %>% filter(wave=="V", stim=="lo", montage=="FzIpsiTiptrode")

source("bayes_crplot.R")

xNameContDia = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "log10NIRCent")
sdBetaContDia = rep(priorSDBeta, length(xNameContDia))
xNameCatDia = c("sexDummy")
sdBetaCatDia = c(10)
numSavedStepsDia = 20000
thinStepsDia = 10
xNameContDiaPretty = c("Age", "PTA 0.5-2 kHz", "PTA 4-12 kHz", "log10TCNE")
xNameCatDiaPretty = c("Sex")

if (updateMCMC == TRUE){
    pg_I_HL_ERL = bayes_crplots(dat=d_I_HL_ERL, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_I_HL_ERL.pdf"))

    pg_I_HL_TPR = bayes_crplots(dat=d_I_HL_TPR, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_I_HL_TPR.pdf"))
    
    pg_I_LL_ERL = bayes_crplots(dat=d_I_LL_ERL, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_I_LL_ERL.pdf"))

    pg_I_LL_TPR = bayes_crplots(dat=d_I_LL_TPR, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_I_LL_TPR.pdf"))

    pg_V_HL_ERL = bayes_crplots(dat=d_V_HL_ERL, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_V_HL_ERL.pdf"))

    pg_V_HL_TPR = bayes_crplots(dat=d_V_HL_TPR, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_V_HL_TPR.pdf"))

    pg_V_LL_ERL = bayes_crplots(dat=d_V_LL_ERL, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_V_LL_ERL.pdf"))

    pg_V_LL_TPR = bayes_crplots(dat=d_V_LL_TPR, yName=yName, xNameCont=xNameContDia, xNameCat=xNameCatDia, sdBetaCont=sdBetaContDia, sdBetaCat=sdBetaCatDia, sds=sds, priorSDBeta=priorSDBeta, numSavedSteps=numSavedStepsDia, thinSteps=thinStepsDia, xNameContPretty=xNameContDiaPretty, xNameCatPretty = xNameCatDiaPretty, ncol=2, plot_width=8, fName=paste0(resDir,"diagnostics/_bayes_cr_plots_V_LL_TPR.pdf"))

}

ft_I_HL_TPR = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_I_HL_TPR)
ft_I_HL_ERL = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_I_HL_ERL)
ft_I_LL_TPR = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_I_LL_TPR)
ft_I_LL_ERL = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_I_LL_ERL)

ft_V_HL_TPR = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_V_HL_TPR)
ft_V_HL_ERL = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_V_HL_ERL)
ft_V_LL_TPR = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_V_LL_TPR)
ft_V_LL_ERL = lm(peakLatencyMeasure~ageDecadeCent+audio0.5_2kHzCent+audio4_12kHzCent+sexDummy+log10NIRCent, data=d_V_LL_ERL)

pdf(paste0(resDir,"diagnostics/_CR_plot_I_HL_TPR.pdf"), width=size2c, height=size2c)
car::crPlots(ft_I_HL_TPR)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_I_HL_ERL.pdf"), width=size2c, height=size2c)
car::crPlots(ft_I_HL_ERL)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_I_LL_TPR.pdf"), width=size2c, height=size2c)
car::crPlots(ft_I_LL_TPR)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_I_LL_ERL.pdf"), width=size2c, height=size2c)
car::crPlots(ft_I_LL_ERL)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_V_HL_TPR.pdf"), width=size2c, height=size2c)
car::crPlots(ft_V_HL_TPR)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_V_HL_ERL.pdf"), width=size2c, height=size2c)
car::crPlots(ft_V_HL_ERL)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_V_LL_TPR.pdf"), width=size2c, height=size2c)
car::crPlots(ft_V_LL_TPR)
dev.off()

pdf(paste0(resDir,"diagnostics/_CR_plot_V_LL_ERL.pdf"), width=size2c, height=size2c)
car::crPlots(ft_V_LL_ERL)
dev.off()
