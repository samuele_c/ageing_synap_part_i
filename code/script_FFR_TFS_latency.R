source("global_parameters.R")
source("utils.R")
source("model_code_robust_fixed_shrinkage.R")
library(dplyr)
library(ggplot2)
##library(BayesUtils)
library(RColorBrewer)
library(psychophysics)
library(lme4)
library(lmerTest)
library(emmeans)
library(xtable)
source("DBDA2E-utilities_DF.R")
source("DBDA2E-utilities_SC.R")

##numSavedSteps=1000; thinSteps=10
numSavedSteps=75000; thinSteps=15
updateMCMC = FALSE
priorSDBeta = 0.1
credMass = 0.99

resDir = paste0(rootResDir, "model_FFR_TFS_latency/")
dir.create(resDir, recursive=T)
dir.create(paste0(resDir, "diagnostics/"), recursive=T)

#######################################
##Load and prep data
#######################################
dat = read.table(paste0(rootDataDir, "FFR_latency_TFSSL.csv"), header=T, sep=",")

subjDatabaseCompl = read.table(paste0(rootDataDir, "demographic.csv"), sep=",", header=T)
dDescr = read.table(paste0(rootDataDir, "descriptives.csv"), sep=",", header=T)
dNIR = read.table(paste0(rootDataDir, "NIR.csv"), sep=",", header=T)
dAudio = read.table(paste0(rootDataDir, "audio_by_freq_no_ear.csv"), sep=",", header=T)

dat = left_join(dat, subjDatabaseCompl %>% select(id, age, ageDecade, gender))
dat = left_join(dat, dAudio %>% select(id, audio1_2kHz=PTA1_2kHz))
dat = left_join(dat, dNIR %>% select(id, log10NIR=log10Total))
dat = left_join(dat, subjDatabaseCompl %>% select(id, musicYearsCubeRoot))

dat$ageDecade = dat$age/10
dat$ageDecadeCent = dat$ageDecade - mean(dat$ageDecade)
##dat$audio1_2kHz = (dat$audio1kHz+dat$audio2kHz)/2
dat$audio1_2kHzCent = dat$audio1_2kHz - mean(dat$audio1_2kHz)
dat$log10NIRCent = dat$log10NIR - mean(dat$log10NIR)
dat$musicYearsCubeRootCent = dat$musicYearsCubeRoot - dDescr$meanMusicYearsCubeRoot
dat$modAmpDummy = ifelse(dat$modAmp == 0.7, -1, 1)
q = model.matrix(~montage, dat, contrasts = list(montage = "contr.sum"))
dat$montDummy1 = as.numeric(q[,2])
dat$montDummy2 = as.numeric(q[,3])
dat$montDummy3 = as.numeric(q[,4])

## ################################
## Set up model variables
## ################################
yName = "lat_alg1"
subjIDName = "id"

xNameCont = c("ageDecadeCent", "audio1_2kHzCent", "log10NIRCent", "musicYearsCubeRootCent")
sdBetaCont = rep(priorSDBeta, length(xNameCont))

xNameCat = c("montDummy1", "montDummy2", "montDummy3", "modAmpDummy")
sdBetaCat = rep(10,length(xNameCat))

xNameInt = list(c("montDummy1", "ageDecadeCent"),
                c("montDummy1", "audio1_2kHzCent"),
                c("montDummy1", "log10NIRCent"),

                c("montDummy2", "ageDecadeCent"),
                c("montDummy2", "audio1_2kHzCent"),
                c("montDummy2", "log10NIRCent"),

                c("montDummy3", "ageDecadeCent"),
                c("montDummy3", "audio1_2kHzCent"),
                c("montDummy3", "log10NIRCent")
                )
xNameIntVarType = list(c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),

                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont"),

                       c("cat", "cont"),
                       c("cat", "cont"),
                       c("cat", "cont")
                       )

sdBetaInt = rep(priorSDBeta, length(xNameInt))
sdBeta = c(sdBetaCont, sdBetaCat, sdBetaInt)

sds = list()
sds[["ageDecadeCent"]] = dDescr$sdAgeDecade
sds[["audio1_2kHzCent"]] = dDescr$sdPTA1_2kHz
sds[["log10NIRCent"]] = dDescr$sdLog10NIR
sds[["musicYearsCubeRootCent"]] = dDescr$sdMusicYearsCubeRoot
sdy = sd(dat[,yName], na.rm=T)


if (updateMCMC == TRUE){

    out = genMCMC(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
                  xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
                  subjIDName=subjIDName,
                  sdBeta=sdBeta,
                  numSavedSteps=numSavedSteps, thinSteps=thinSteps)
    mcmcCoda = out[[1]]
    runJagsOut = out[[2]]
    varNames = out[[3]]
    saveRDS(mcmcCoda, file=paste0(resDir, "mcmcCoda.RDS"))
    saveRDS(runJagsOut, file=paste0(resDir, "runJagsOut.RDS"))
    saveRDS(varNames, file=paste0(resDir, "varNames.RDS"))

    parameterNames = varnames(mcmcCoda) # get all parameter names
    for (parName in parameterNames) {
        pdf(paste0(resDir, "diagnostics/", parName, ".pdf"))
        diagMCMC(codaObject=mcmcCoda , parName=parName)
        dev.off()
    }
} else {

   mcmcCoda = readRDS(file=paste0(resDir, "mcmcCoda.RDS"))
    runJagsOut = readRDS(file=paste0(resDir, "runJagsOut.RDS"))
    varNames = readRDS(file=paste0(resDir, "varNames.RDS"))
}


mcmcMat = as.matrix(mcmcCoda, chains=T)

pdf(paste0(resDir, "posterior.pdf"))
par(mfrow=c(2,3))
plotPost(mcmcMat[, "Intercept"], main="Intercept", credMass=credMass)
for (i in 1:length(varNames)){
    plotPost(mcmcMat[, varNames[i]], main=varNames[i], compVal=0, credMass=credMass)
}
dev.off()

pdf(paste0(resDir, "posterior_z.pdf"))
par(mfrow=c(2,3))
plotPost(mcmcMat[, "z_Intercept"], main="Intercept", credMass=credMass)
for (i in 1:length(varNames)){
    plotPost(mcmcMat[, paste0("z_", varNames[i])], main=varNames[i], compVal=0, credMass=credMass)
}
dev.off()

b_Age = mcmcMat[, "ageDecadeCent"]
b_NIR = mcmcMat[, "log10NIRCent"]
b_PTA = mcmcMat[, "audio1_2kHzCent"]
b_Mus = mcmcMat[, "musicYearsCubeRootCent"]
b_M1 = mcmcMat[, "montDummy1"]
b_M2 = mcmcMat[, "montDummy2"]
b_M3 = mcmcMat[, "montDummy3"]
b_M1xAge = mcmcMat[, "montDummy1XageDecadeCent"]
b_M2xAge = mcmcMat[, "montDummy2XageDecadeCent"]
b_M3xAge = mcmcMat[, "montDummy3XageDecadeCent"]
b_M1xNIR = mcmcMat[, "montDummy1Xlog10NIRCent"]
b_M2xNIR = mcmcMat[, "montDummy2Xlog10NIRCent"]
b_M3xNIR = mcmcMat[, "montDummy3Xlog10NIRCent"]
b_M1xPTA = mcmcMat[, "montDummy1Xaudio1_2kHzCent"]
b_M2xPTA = mcmcMat[, "montDummy2Xaudio1_2kHzCent"]
b_M3xPTA = mcmcMat[, "montDummy3Xaudio1_2kHzCent"]

zb_Age = mcmcMat[, "z_ageDecadeCent"]
zb_Noise = mcmcMat[, "z_log10NIRCent"]
zb_Aud = mcmcMat[, "z_audio1_2kHzCent"]
zb_Mus = mcmcMat[, "z_musicYearsCubeRootCent"]

mont1_age = b_Age + b_M1xAge
mont2_age = b_Age + b_M2xAge
mont3_age = b_Age + b_M3xAge
mont4_age = b_Age - (b_M1xAge+b_M2xAge+b_M2xAge)

mont1_NIR = b_NIR + b_M1xNIR
mont2_NIR = b_NIR + b_M2xNIR
mont3_NIR = b_NIR + b_M3xNIR
mont4_NIR = b_NIR - (b_M1xNIR+b_M2xNIR+b_M2xNIR)

mont1_PTA = b_PTA + b_M1xPTA
mont2_PTA = b_PTA + b_M2xPTA
mont3_PTA = b_PTA + b_M3xPTA
mont4_PTA = b_PTA - (b_M1xPTA+b_M2xPTA+b_M2xPTA)


## #######################
## Summary Plots
## #######################

df_post = summarizePostListDF(list(b_Age, mont1_age, mont2_age, mont3_age, mont4_age,
                                   b_PTA, mont1_PTA, mont2_PTA, mont3_PTA, mont4_PTA,
                                   b_NIR, mont1_NIR, mont2_NIR, mont3_NIR, mont4_NIR,
                                   b_Mus), labels=c("b_age", "mont1_age", "mont2_age", "mont3_age", "mont4_age",
                                                    "b_PTA", "mont1_PTA", "mont2_PTA", "mont3_PTA", "mont4_PTA",
                                                    "b_NIR", "mont1_NIR", "mont2_NIR", "mont3_NIR", "mont4_NIR",
                                                    "b_Mus"), credMass=credMass)
df_post$montage = c(rep(c("Acr. Mnt.", "HF-C7", "HF-LERL", "HF-LMST", "HF-LTPR"), 3), "Acr. Mnt.")
df_post$eff = c(rep(c("Age", "PTA", "NIR"), each=5), "Music")

p = ggplot(df_post, aes(x=montage, y=Median)) + geom_point(shape=1) + facet_wrap(~eff)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + theme_bw2 + theme(axis.title.x = element_text(hjust=1))
p = p + ylab("SNR Change (dB) X Dep. Var. Change")
p = p + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
ggsave(filename=paste0(resDir, "HDIs.pdf"), width=size2c, height=size2c)

writeLines(capture.output(sessionInfo()), "session_info/session_info_FFR_TFS_latency.txt")
## df_post = summarizePostListDF(list(b_Age, b_Aud, b_Noise, b_Mus),
##                                   labels=c("Age Decade", "PTA (dB)", "Noise Exp.", "Music"), credMass=credMass)

## df_z_post = summarizePostListDF(list(zb_Age, zb_Aud, zb_Noise, zb_Mus),
##                                labels=c("Age", "PTA", "Noise Exp.", "Music"), credMass=credMass)



## p = ggplot(df_post, aes(x=label, y=Median)) + geom_point(shape=1)
## p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
## p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
## p = p + theme_bw2 + theme(axis.title.x = element_text(hjust=1))
## p = p + ylab("SNR PC1 Change (s.d.) X Dep. Var. Change")
## p = p + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
## ggsave(filename=paste0(resDir, "HDIs.pdf"), width=3.4252, height=3.4252)

## p = ggplot(df_z_post, aes(x=label, y=Median)) + geom_point(shape=1)
## p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
## p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
## p = p + theme_bw2 + theme(axis.title.x = element_text(hjust=1)) 
## p = p + ylab("SNR PC1 Change (s.d.) X Dep. Var. Change (s.d.)") + xlab(NULL) + theme(axis.title.x = element_text(hjust=1))
## ggsave(filename=paste0(resDir, "HDIs_z.pdf"), width=3.4252, height=3.4252)


## write.table(df_post, file=paste0(resDir, "df_post.csv"), col.names=T, row.names=F, sep=",")
## write.table(df_z_post, file=paste0(resDir, "df_z_post.csv"), col.names=T, row.names=F)


## emm_options(pbkrtest.limit = 3264)
## options(contrasts=c('contr.sum','contr.poly'))
## fit1 = lmer(zFFRPC1 ~ ageDecadeCent + audio1_2kHzCent + log10NIRCent + (1|id), data=dat)

## ## ## do pairwise ~ factorName if you want the contrasts
## ## #emtrends(fit1, pairwise ~ component, var = "ageDecadeCent")
## ## ageTrendsByComp = emtrends(fit1, ~ component, var = "ageDecadeCent")
## ## confint(ageTrendsByComp, level=0.99)
## ## noiseTrendsByComp = emtrends(fit1, ~ component, var = "log10NIRCent")


## source("getScalingFacs.R")

## scalFacs = getScalingFacs(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
##                   xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
##                   subjIDName=subjIDName,
##                   sdBeta=sdBeta,
##                   numSavedSteps=numSavedSteps, thinSteps=thinSteps)

## nVars = length(varNames)
## vNamesArr = character(nVars)
## cenTendArr = numeric(nVars)
## CIArr = character(nVars)
## nDig = 3
## zsdBetaArr = c(sdBetaCont, sdBetaCat, sdBetaInt)
## pType = c(rep("Cont.", length(xNameCont)), rep("Cat.", length(xNameCat)), rep("Int.", length(xNameInt)))
## for (vn in 1:length(varNames)){
##     thisVar = varNames[vn]
##     thisPost = summarizePost(mcmcMat[,thisVar], credMass=credMass)
##     vNamesArr[vn] = thisVar
##     cenTendArr[vn] = thisPost[["Median"]]
##     CIArr[vn] = paste0(as.character(round(thisPost[["HDIlow"]], nDig)), "--", as.character(round(thisPost[["HDIhigh"]], nDig)))
## }

## ptns = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "audio1_2kHzCent", "log10NIRCent", "stimDummy", "waveDummy", "montDummy", "sexDummy", "X", "carrFreqDummy", "modAmpDummy", "musicYearsCubeRootCent")
## rpls = c("A", "PTA0.5--2", "PTA4--12", "PTA1--2", "log10TCNE", "L", "W", "M", "S", "x", "CF", "MA", "MY")
## for (i in 1:length(ptns)){
##     vNamesArr = gsub(ptns[i], rpls[i], vNamesArr)
## }
## df_coeffs = data.frame(varName=vNamesArr, pType, zsdBeta=zsdBetaArr, sdBeta=zsdBetaArr/scalFacs*sdy, Median=cenTendArr, CI=CIArr)

## xt = xtable(df_coeffs, digits=3)
## writeLines(print(xt), paste0(resDir, "coeffs.tex"))


source("getScalingFacs.R")

scalFacs = getScalingFacs(dat=dat, xNameCont=xNameCont, xNameCat=xNameCat,
                  xNameInt=xNameInt, xNameIntVarType=xNameIntVarType, yName=yName,
                  subjIDName=subjIDName,
                  sdBeta=sdBeta,
                  numSavedSteps=numSavedSteps, thinSteps=thinSteps)

varNames2 = c("Intercept", varNames)
nVars = length(varNames2)
vNamesArr = character(nVars)
cenTendArr = numeric(nVars)
CIArr = character(nVars)
nDig = 3
zsdBetaArr = c(sdBetaCont, sdBetaCat, sdBetaInt)
pType = c("", rep("Cont.", length(xNameCont)), rep("Cat.", length(xNameCat)), rep("Int.", length(xNameInt)))
for (vn in 1:nVars){
    thisVar = varNames2[vn]
    thisPost = summarizePost(mcmcMat[,thisVar], credMass=credMass)
    vNamesArr[vn] = thisVar
    cenTendArr[vn] = thisPost[["Median"]]
    CIArr[vn] = paste0(as.character(round(thisPost[["HDIlow"]], nDig)), "--", as.character(round(thisPost[["HDIhigh"]], nDig)))
}

ptns = c("ageDecadeCent", "audio0.5_2kHzCent", "audio4_12kHzCent", "audio1_2kHzCent", "log10NIRCent", "stimDummy", "waveDummy", "montageDummy", "montDummy", "sexDummy", "X", "carrFreqDummy", "modAmpDummy", "musicYearsCubeRootCent")
rpls = c("A", "PTA0.5--2", "PTA4--12", "PTA1--2", "log10TCNE", "L", "W", "M", "M", "S", "x", "CF", "MA", "MY")
for (i in 1:length(ptns)){
    vNamesArr = gsub(ptns[i], rpls[i], vNamesArr)
}
df_coeffs = data.frame(varName=vNamesArr, pType, zsdBeta=c(2, zsdBetaArr), sdBeta=c(2*sdy, zsdBetaArr/scalFacs*sdy), Median=cenTendArr, CI=CIArr)

xt = xtable(df_coeffs, digits=3)
writeLines(print(xt, include.rownames=FALSE), paste0(resDir, "coeffs.tex"))
