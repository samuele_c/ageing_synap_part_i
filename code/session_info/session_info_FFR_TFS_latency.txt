R version 3.6.2 (2019-12-12)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux 10 (buster)

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/openblas/libblas.so.3
LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.3.5.so

locale:
 [1] LC_CTYPE=it_IT.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=it_IT.UTF-8        LC_COLLATE=it_IT.UTF-8    
 [5] LC_MONETARY=it_IT.UTF-8    LC_MESSAGES=it_IT.UTF-8   
 [7] LC_PAPER=it_IT.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=it_IT.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] xtable_1.8-4        emmeans_1.4.3.01    lmerTest_3.1-1     
 [4] lme4_1.1-21         Matrix_1.2-18       RColorBrewer_1.1-2 
 [7] BayesUtils_1.0.9    metRology_0.9-28-1  rstan_2.19.2       
[10] StanHeaders_2.19.0  psychophysics_1.0.7 rjags_4-10         
[13] coda_0.19-3         dplyr_0.8.3         runjags_2.0.4-6    
[16] ggplot2_3.2.1      

loaded via a namespace (and not attached):
 [1] tidyselect_0.2.5    purrr_0.3.3         splines_3.6.2      
 [4] lattice_0.20-38     colorspace_1.4-1    stats4_3.6.2       
 [7] loo_2.2.0           rlang_0.4.2         pkgbuild_1.0.6     
[10] pillar_1.4.3        nloptr_1.2.1        glue_1.3.1         
[13] withr_2.1.2         matrixStats_0.55.0  lifecycle_0.1.0    
[16] robustbase_0.93-5   munsell_0.5.0       gtable_0.3.0       
[19] mvtnorm_1.0-11      labeling_0.3        inline_0.3.15      
[22] callr_3.4.0         ps_1.3.0            parallel_3.6.2     
[25] fansi_0.4.0         DEoptimR_1.0-8      Rcpp_1.0.3         
[28] scales_1.1.0        farver_2.0.1        gridExtra_2.3      
[31] processx_3.4.1      numDeriv_2016.8-1.1 grid_3.6.2         
[34] cli_2.0.0           tools_3.6.2         magrittr_1.5       
[37] lazyeval_0.2.2      tibble_2.1.3        crayon_1.3.4       
[40] pkgconfig_2.0.3     MASS_7.3-51.4       prettyunits_1.0.2  
[43] estimability_1.3    assertthat_0.2.1    minqa_1.2.4        
[46] R6_2.4.1            boot_1.3-23         nlme_3.1-143       
[49] compiler_3.6.2     
